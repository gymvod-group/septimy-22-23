## 1. Algoritmy - největší společný dělitel
- Určete největšího společného dělitele čísel x a y
- Čísla x a y zadá uživatel 
- Určete počet jejich společných dělitelů
- Nezapomeňte zkontrolovat všechny vstupy a vypsat na obrazovku toto číslo.

**Dobrovolný domácí úkol**: Určete největšího společného dělitele z definice (rozklad na prvočísla).
