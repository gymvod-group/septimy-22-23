## Obsah

Na dnešní hodině si zopakujeme všechny (jednoduché) řadící algoritmy: Bubble sort, Insert sort a Select sort a napsali aplikaci, která je využívá.

## 1. Řadící algoritmy
1) Připravte WinForm aplikaci, která bude umět řadit číselné řady několika způsoby. Aplikace by měla mít následující funkce:
- pole pro zadání počtu čísel (použijte vhodný uživatelský prvek)
- tlačítko, které vygeneruje daný počet (viz bod a) náhodných čísel a vypíše je uživateli
- textové pole, které uživateli umožní zadat čísla "ručně"
- tlačítko, které řadu seřadí a vypíše ji uživateli metodou BubbleSort
- tlačítko, které řadu seřadí a vypíše ji uživateli metodou SelectSort
- tlačítko, které řadu seřadí a vypíše ji uživateli metodou InsertSort
