﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP
{
    class Person
    {
        private int x, y;
        private string name;
        protected string job;
        protected Color color = Color.Blue;


        private int leftLegX;
        private int rightLegX;

        public Person(int x, int y, string name)
        {
            this.x = x;
            this.y = y;
            leftLegX = x + 30;
            rightLegX = x + 0;
            this.name = name;
        }

        public void Draw(Graphics g)
        {
            SolidBrush brush = new SolidBrush(color);
            Pen pen = new Pen(color, 3);
            Pen penGreen = new Pen(Color.Green, 3);

            g.FillEllipse(brush, x, y, 30, 30);
            g.DrawLine(pen, x+15, y+30, x + 15, y + 60);
            //ruce
            g.DrawLine(pen, x + 15, y + 30, x + 0, y + 60);
            g.DrawLine(pen, x + 15, y + 30, x + 30, y + 60);

            //nohy
            g.DrawLine(pen, x+15, y+60, rightLegX, y + 100);
            g.DrawLine(penGreen, x+15, y+60, leftLegX, y + 100);

            //jmeno
            PrintName(g);
        }

        public void Move(int xShift, int yShift)
        {
            this.x += xShift;
            this.y += yShift;

            leftLegX += xShift;
            rightLegX += xShift;


            int temp = leftLegX;
            leftLegX = rightLegX;
            rightLegX = temp;
        }

        public void SayHi()
        {
            MessageBox.Show($"{name} zdraví");
        }

        public void SayJob()
        {
            MessageBox.Show($"{name} pracuje jako {job}");
        }

        private void PrintName(Graphics g)
        {
            Font drawFont = new Font("Arial", 16);
            SolidBrush drawBrush = new SolidBrush(Color.Black);
            StringFormat drawFormat = new StringFormat();
            g.DrawString(name, drawFont, drawBrush, x - 10, y - 20, drawFormat);
        }
    }
}
