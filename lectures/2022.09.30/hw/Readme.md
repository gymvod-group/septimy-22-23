# Základní operace s vektory

## Deadline: 7.10.2022...

Vytvořte konzolovou aplikaci, která umožní provádět základní operace s vektory. 
Aplikace na vstupu nejdříve příjme dimenzi vektoru n a pak načte dva vektory u a v. Tyto vektory budou mít dimenzi rovnu n.
Aplikace pak provede základní operace: sčítání, odčítání, velikost a skalární součin. Nakonec napíšete, jestli jsou vektory na sebe kolmé.

### př.: 

Kolik prvků budou mít vektory?
3
Zadejte vektor u.
1
2
3
Zadejte vektor v.
3
2
1

Součet vektorů je:
(4, 4, 4)
Rozdíl vektorů je:
(-2, 0, 2)
Velikost vektoru u je:
3.74166
Skalární součin je:
10
Vektory na sebe nejsou kolmé.
