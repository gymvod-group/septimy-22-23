﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvancedSort2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[]{38, 27, 43, 3, 9, 82, 10};
            Console.WriteLine("Given array");
            Console.WriteLine(String.Join(", ", arr));

            //sort
            MergeSort mergeTest = new MergeSort();
            mergeTest.Sort(arr, 0, arr.Length - 1);

            Console.WriteLine();
            Console.WriteLine("Sorted array");
            Console.WriteLine(String.Join(", ", arr));

            Console.ReadKey();

            Array.Copy();



        }
    }
}
