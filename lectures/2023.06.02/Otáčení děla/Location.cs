﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otáčení_děla
{
    abstract class Location
    {
        public double X { get; protected set; }
        public double Y { get; protected set; }

    }
}
