Na vypracování máte 60 minut.
Některé odpovědi se mohou lišit v jiných programovacích jazycích, ptám se na C# a "svět" .NET Frameworku.

a) Jaké jsou tři základní řídicí struktry algoritmu a co znamenají?

b) Co jsou to kompilované jazyky? Zamyslete se nad výhodami i nevýhodami.

c) Popište dva datové typy, do kterých můžeme ukládat soubor dat stejného datového typu, a popište, v čem se liší?

d) Jaké znáte druhy cyklů? Popište je.

e) Co je to string builder a v čem je lepší než samotný string?

f) Co je to implicitní konverze? Uveďte příklad implicitní konverze. Vysvětlete, kdy může nastat při konverzi Overflow exception.

g) Jak se jmenuje třída, která slouží pro zapisování do souboru a jak se používá (3 kroky)? 

h) Jak definujeme funkci bez návratové hodnoty a jak ji zavoláme?

i) Jaké znáte řadící algoritmy?

j) Co je to OOP? Vysvětlete, z čeho se skládá třída a co znamená zapouzdření. Jak se liší private, protected a public.

k) Co je to overload a co je to override? Vysvětlete, jak toho docílíme v C#.

l) Převody mezi binární a desítkovou soustavou. Zaznamenejte i postup, aby bylo zřejmé, jak jste k řešení dospěli.
	1) 135
	2) 1101101

m) Jak získám kreslící plátno (Graphics).

N) Co známená, když je třída abstraktní?