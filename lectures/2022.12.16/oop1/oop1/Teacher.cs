﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop1
{
    class Teacher : Person
    {
        public Teacher(int x, int y, string name) : base(x, y, name)
        {
            this.job = "Teacher";
        }
    }
}
