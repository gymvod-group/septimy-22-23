### Obsah

## Opáčko

Customer customer = new Customer()?

Co je to abstraktní třída?

Co je to string builder a v čem je lepší než samotný string?




## 1. Práce s poli a seznamy
- Vytvořte konzolovou aplikaci, která bude obsahovat následující algoritmus:
- Vytvořte pole C celých čísel o 50 prvcích a vygenerujte do něj náhodná čísla mezi 1 a 50.
- Vytvořte další seznam celých čísel P - prvočísla a dvě celočíselné proměnné SP a SC.
- Projděte všechny prvky pole C a prvočísla vložte do seznamu P.
- Součet čísel v seznamu P uložte do proměnné SP a součet čísel seznamu C do proměnné SC.
- Obsah polí C a P a proměnné SP a SC vypište do konzole.


## 2. Rekurzivní výpočet Fibonacciho posloupnosti
- Vygenerujte Fibonacciho posloupnost !rekurzivně! pro čísla od 1 do 40


## 3. Výdaje
Připravte program, který v reakci na výběr Soubor > otevřít z CSV, načte data. 
Tyto údaje zobrazí v tabulce, kde je může uživatel editovat 
a poté z nabídky Operace > Spočti spustit sečtení.


