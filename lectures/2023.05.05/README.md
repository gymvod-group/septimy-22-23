### Obsah

## Opáčko

Customer customer = new Customer()?

Co je to abstraktní třída?

Co je to string builder a v čem je lepší než samotný string?



## 1. AirTraffic

- Napište program, který umožní vytvořit nový objekt Plane s parametry „Rok výroby“, „Typ“ a „Kapacita“.
- Tyto atributy budou privátní. Třída Letadlo, která bude tyto atributy spravovat, bude obsahovat také metodu pro přidávání cestujících a implementovanou metodu ToString(). Metoda pro přidávání cestujících ošetří vstup tak, aby se nemohlo stát, že bude překročena kapacita letadla.

- Uživatelské rozhraní bude tvořit ListBox, který bude zobrazovat letadla.
- Přidáme tlačítka na přidaní letadla a odebraní letadla z listboxu.

- Pro vybrané letadlo v listboxu dalším tlačítkem vypišíme jeho atributy do textBoxu a dalším tlačítkem pro stejně vybrané letadlo přidáme cestujícího.


## 2. Práce s poli a seznamy
- Vytvořte konzolovou aplikaci, která bude obsahovat následující algoritmus:
- Vytvořte pole C celých čísel o 50 prvcích a vygenerujte do něj náhodná čísla mezi 1 a 50.
- Vytvořte další seznam celých čísel P - prvočísla a dvě celočíselné proměnné SP a SC.
- Projděte všechny prvky pole C a prvočísla vložte do seznamu P.
- Součet čísel v seznamu P uložte do proměnné SP a součet čísel seznamu C do proměnné SC.
- Obsah polí C a P a proměnné SP a SC vypište do konzole.


## 3. Rekurzivní výpočet Fibonacciho posloupnosti
- Vygenerujte Fibonacciho posloupnost !rekurzivně! pro čísla od 1 do 40

