﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace roman2
{
    public partial class Form1 : Form
    {
        IRomanConverter converter;

        public Form1()
        {
            InitializeComponent();

            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("po", "pondeli");
            dic.Add("ut", "utery");
            dic.Add("st", "streda");
            dic.Add("ct", "ctvrtek");
            dic.Add("pa", "patek");

            label1.Text = dic["pa"];

            //label1.Text = dic["so"];
            if (dic.ContainsKey("so"))
                label1.Text = dic["so"];

            dic.Remove("pa");
            //label1.Text = dic["pa"];


            label1.Text = "";
            foreach (KeyValuePair<string, string> item in dic)
            {
                label1.Text += $"Key={item.Key}, Value={item.Value}; ";
            }


            converter = new Converter();
        }

        private void buttonToRoman_Click(object sender, EventArgs e)
        {
            string input = textBox1.Text;
            int number;
            if (!int.TryParse(input, out number))
                labelResult.Text = "chyba";


            labelResult.Text = converter.ToRoman(number);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string input = textBox1.Text;

            labelResult.Text = converter.FromRoman(input).ToString();
        }
    }
}
