﻿namespace morseovka
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxIn = new System.Windows.Forms.TextBox();
            this.buttonCzech = new System.Windows.Forms.Button();
            this.buttonMorse = new System.Windows.Forms.Button();
            this.textBoxOut = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxIn
            // 
            this.textBoxIn.Location = new System.Drawing.Point(13, 13);
            this.textBoxIn.Multiline = true;
            this.textBoxIn.Name = "textBoxIn";
            this.textBoxIn.Size = new System.Drawing.Size(255, 41);
            this.textBoxIn.TabIndex = 0;
            // 
            // buttonCzech
            // 
            this.buttonCzech.Location = new System.Drawing.Point(13, 60);
            this.buttonCzech.Name = "buttonCzech";
            this.buttonCzech.Size = new System.Drawing.Size(114, 23);
            this.buttonCzech.TabIndex = 1;
            this.buttonCzech.Text = "Prelozit do cestiny";
            this.buttonCzech.UseVisualStyleBackColor = true;
            this.buttonCzech.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonMorse
            // 
            this.buttonMorse.Location = new System.Drawing.Point(133, 60);
            this.buttonMorse.Name = "buttonMorse";
            this.buttonMorse.Size = new System.Drawing.Size(135, 23);
            this.buttonMorse.TabIndex = 2;
            this.buttonMorse.Text = "Prelozit do morseovky";
            this.buttonMorse.UseVisualStyleBackColor = true;
            this.buttonMorse.Click += new System.EventHandler(this.buttonMorse_Click);
            // 
            // textBoxOut
            // 
            this.textBoxOut.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxOut.Location = new System.Drawing.Point(13, 90);
            this.textBoxOut.Multiline = true;
            this.textBoxOut.Name = "textBoxOut";
            this.textBoxOut.Size = new System.Drawing.Size(255, 48);
            this.textBoxOut.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBoxOut);
            this.Controls.Add(this.buttonMorse);
            this.Controls.Add(this.buttonCzech);
            this.Controls.Add(this.textBoxIn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxIn;
        private System.Windows.Forms.Button buttonCzech;
        private System.Windows.Forms.Button buttonMorse;
        private System.Windows.Forms.TextBox textBoxOut;
    }
}

