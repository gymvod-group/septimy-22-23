# Septimy 22-23

GYMVOD programovací seminář Septimy 2022/2023

## Učitelé

**David Hájek**

- hajekdd@gmail.com
- FEL
- Java, php, javascript, python


### TEST
- 03.03.2023 ze všeho!


### Online



### Soutěž Kasiopea
- https://kasiopea.matfyz.cz/


### Pravidla

- Všichni se naučíme pracovat s GITem. Materiály budou na GitLabu.
- Discord - https://discord.gg/ZwSBbRBnW9
- Úkoly budou každý týden nebo ob týden. Na vypracování máte týden,
s dobrou výmluvou máte dva týdny. Jinak dostanete úkol navíc. Za
úkoly se budou dostávat jedničky. Neopisovat, poznám to. Odevzdávat
na svůj repositář.
- Budete mít projekt na semestr ve dvojicích. 
- Dvě až tři písemky za pololetí (teoretická a praktická část).
- Při hodině budete spolupracovat.

### Harmonogram 

| Datum | Učitel | Téma | Přednášky | Úkol |
| --- | --- | --- | --- | --- |
| 2.9.2022 | David | Úvod; seznámení; stránka; úkoly; semestrálka; postup při řešení problému; vlastnosti algoritmu, základní struktury; basic úloha | [lec01](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.09.02/uvodni_prezentace.pdf) | |
| 9.9.2022 | David | | | |
| 16.9.2022 | David | GIT, historie, lokální práce | [lec02](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.09.16/git-uvod.pdf) | |
| 23.9.2022 | David | Gitlab, Progamovací jazyky, kvadratická rovnice  | [lec03](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.09.23/) | [hw01](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.09.23/hw)|
| 23.9.2022 | David | Datové typy | [lec04](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.09.30/) | [hw02](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.09.30/hw)|
| 7.10.2022 | David | Procvičování | [lec05](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.10.07/) | |
| 14.10.2022 | David | Základní struktury | [lec06](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.10.14/) | |
| 21.10.2022 | David | Cyklus a rekurze | [lec07](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.10.21/) | [hw03](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.10.21/hw) |
| 28.10.2022 | David | Volno | | |
| 04.11.2022 | David | Rekurze a seznamy| [lec08](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.11.04/) | [hw04](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.11.04/hw) |
| 11.11.2022 | David | Test | | |
| 18.11.2022 | David | Volno | | |
| 25.11.2022 | David | String a soubory | [lec09](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.11.25/) | [hw05](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.11.25/hw) |
| 02.12.2022 | David | Sorting | [lec10](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.12.02/) | [hw06](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.12.02/hw) |
| 09.12.2022 | David | Sorting  + gui| [lec11](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.12.09/) |  |
| 16.12.2022 | David | OOP | [lec12](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.12.16/) | [hw08](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2022.12.16/hw)  |
| 06.01.2023 | David | OOP 2 | [lec13](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.01.06/) |  |
| 13.01.2023 | David | Snowstorm | [lec14](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.01.13/) |  |
| 20.01.2023 | David | Eratosthen | [lec15](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.01.20/) |  |
| 27.01.2023 | David | Dictionary | [lec16](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.01.27/) |  |
| 17.02.2023 | David | Aritmetika | [lec17](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.02.17/) | [hw09](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.02.17/hw) |
| 24.02.2023 | David | Merge sort | [lec18](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.02.24/) | |
| 03.03.2023 | David | Testik | [lec19](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.03.03/) | |
| 10.03.2023 | David | Morseovka | [lec20](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.03.10/) | [hw10](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.03.10/hw) |
| 17.03.2023 | David | Grafika | [lec21](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.03.17/) |  |
| 24.03.2023 | David | Práce se soubory | [lec22](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.03.24/) |  |
| 31.03.2023 | David | Závorky | [lec23](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.03.31/) |  |
| 14.04.2023 | David | Vektorovy obrazek a GCD | [lec24](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.04.14/) |  |
| 21.04.2023 | David | GCD, integrál a datové typy | [lec25](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.04.21/) |  |
| 28.04.2023 | David | Řadící algo | [lec26](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.04.28/) |  |
| 05.05.2023 | David | AirTraffic | [lec27](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.05.05/) |  |
| 12.05.2023 | David | Výdaje | [lec28](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.05.12/) |  |
| 19.05.2023 | David | Test | [lec29](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.05.19/) |  |
| 26.05.2023 | David | Semestrálky | [lec30](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.05.26/) |  |
| 26.05.2023 | David | Točení děla a semestrálky | [lec31](https://gitlab.com/gymvod-group/septimy-22-23/-/blob/main/lectures/2023.06.02/) |  |


